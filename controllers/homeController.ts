import { Request, Response } from "express";


const home = function(req: Request, res: Response) {
    res.status(200).json({
        message: 'Welcome'
    });
};


export default {
    home
}