
import Server from './classes/server';
import homeRoutes from './routes/home';
import mongoose from 'mongoose';

import bodyParser from 'body-parser';
import cors from 'cors';


const server = new Server();

const whitelist = ['localhost', '127.0.0.1']
var corsOptions = {
  origin: function (origin: any , callback: Function ) {
    if (whitelist.indexOf(origin) !== -1) {
        callback(null, true)
    } else {
    //   callback(new Error('Not allowed by CORS'))
        callback(null, false)
    }
  },
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}

server.app.use( cors(corsOptions) );

// Body parser
server.app.use( bodyParser.urlencoded({ extended:true }) );
server.app.use( bodyParser.json() );

// rutas
server.app.use('/', homeRoutes);

var options = { server: { socketOptions: { keepAlive: 60000, connectTimeoutMS: 30000 } },
                replset: { socketOptions: { keepAlive: 60000, connectTimeoutMS : 30000 } } }

mongoose.connect('mongodb://localhost:27017/university', 
                { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true}, ( error) => {
                    if (error) throw error;

                    console.log('Base de datos online');
                });

// levantar express
server.start( () => {
    console.log('Servidor corriendo en puerto 3000');
});