"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var server_1 = __importDefault(require("./classes/server"));
var home_1 = __importDefault(require("./routes/home"));
var mongoose_1 = __importDefault(require("mongoose"));
var body_parser_1 = __importDefault(require("body-parser"));
var cors_1 = __importDefault(require("cors"));
var server = new server_1.default();
var whitelist = ['localhost', '127.0.0.1'];
var corsOptions = {
    origin: function (origin, callback) {
        if (whitelist.indexOf(origin) !== -1) {
            callback(null, true);
        }
        else {
            //   callback(new Error('Not allowed by CORS'))
            callback(null, false);
        }
    },
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
};
server.app.use(cors_1.default(corsOptions));
// Body parser
server.app.use(body_parser_1.default.urlencoded({ extended: true }));
server.app.use(body_parser_1.default.json());
// rutas
server.app.use('/', home_1.default);
var options = { server: { socketOptions: { keepAlive: 60000, connectTimeoutMS: 30000 } },
    replset: { socketOptions: { keepAlive: 60000, connectTimeoutMS: 30000 } } };
mongoose_1.default.connect('mongodb://localhost:27017/university', { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true }, function (error) {
    if (error)
        throw error;
    console.log('Base de datos online');
});
// levantar express
server.start(function () {
    console.log('Servidor corriendo en puerto 3000');
});
