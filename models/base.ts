const baseEntity = {
    status: {
        type: String,
        required: [true, 'El estado es obligatorio']
    },
    created_date: {
        type: Date,
        required: [true, 'La fecha de creacion es obligatoria']
    },
    deleted_date: {
        type: Date,
    }
}


const entity = {
    ...baseEntity,
    name: {
        type: String,
        required: [true, 'El nombre es obligatorio']
    },
    description: {
        type: String
    },
}


export default {
    baseEntity,
    entity
}